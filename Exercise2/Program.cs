﻿using System;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();

            for (var i = 1; i < 4; i++)
            {
                Console.WriteLine(context.createInstance());
            }
            
            context.notASingletonList[2].sayHello();
        }
    }
}