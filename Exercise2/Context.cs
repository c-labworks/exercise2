﻿using System;
using System.Collections.Generic;

namespace Exercise2
{
    public class Context
    {
        public List<NotASingleton> notASingletonList { get; } = new List<NotASingleton>();

        public string createInstance()
        {
            if (notASingletonList.Count > 3) return "Creation not available";

            var name = "Instance №" + notASingletonList.Count;
            notASingletonList.Add(new NotASingleton(name));
            return "Created " + name;
        }

        public class NotASingleton
        {
            private string name { get; set; }
            internal NotASingleton(string name)
            {
                this.name = name;
            }

            public void sayHello()
            {
                Console.WriteLine("Hello, I'm " + name);
            }
        }
    }
}